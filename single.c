#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>

#include "postprocess.h"

void
print_usage(char *name)
{
    fprintf(stderr, "usage: %s [-v] [-q quality] INPUT OUTPUT\n", name);
    fprintf(stderr, "Post-process a single .dng bayer raw file to a jpeg file.\n\n");
    fprintf(stderr, "Mandatory arguments\n");
    fprintf(stderr, "INPUT    Input filename, should be a .dng file\n");
    fprintf(stderr, "OUTPUT   Output filename, should be a .jpg file\n\n");

    fprintf(stderr, "Optional arguments\n");
    fprintf(stderr, "  -v     Show verbose debugging output\n");
    fprintf(stderr, "  -q 90  Set the output jpeg quality [0-100]\n\n");
}

int
main(int argc, char *argv[])
{
    int opt;
    int verbose = 0;
    long temp;
    int quality = 90;
    char *endptr;
    char *input_path;
    char *output_path;

    while ((opt = getopt(argc, argv, "vq:h")) != -1) {
        switch (opt) {
            case 'v':
                verbose = 1;
                break;
            case 'q':
                temp = strtol(optarg, &endptr, 0);
                if (errno == ERANGE || *endptr != '\0' || optarg == endptr) {
                    fprintf(stderr, "Invalid value for -q\n");
                    print_usage(argv[0]);
                    return 1;
                }
                if (temp < 0 || temp > 100) {
                    fprintf(stderr, "Quality out of range, valid values are 0-100\n");
                    print_usage(argv[0]);
                    return 1;
                }
                quality = (int) temp;
                break;
            case 'h':
                print_usage(argv[0]);
                return 0;
            case '?':
                print_usage(argv[0]);
                return 1;
        }
    }

    if (argc < optind + 2) {
        fprintf(stderr, "Missing image parameters\n");
        print_usage(argv[0]);
        return 1;
    }

    input_path = argv[optind];
    output_path = argv[optind + 1];

    postprocess_setup();
    postprocess_single(input_path, output_path, quality, verbose);
    return 0;
}
